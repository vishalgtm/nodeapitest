var express = require('express');
var router = express.Router();
var db=require('../db');

router.post("/",function(req,res){
    req.setTimeout(300000);
    var hsCode=req.body.hsCode;
    var country=req.body.country;
    var fiscalYear=req.body.fiscalYear;
    var portId=req.body.portId;
    var dataType=req.body.dataType;
    db.query('call data_visualization_by_country(?,?,?,?,?)',[dataType,hsCode,country,fiscalYear,portId],
    function(error,results,fields){
        
        if(error) console.log(error);
        res.send(JSON.stringify({"status":200,"error":null,"response":results}));

    });
   
});

module.exports=router;